<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;


    public function setUp() :void
    {
        parent::setUp();

        $user = new User([
            'name'     => 'Bradley Yarrow',
            'email'    => 'yarrowbradley@gmail.com',
            'password' => '12345678'
        ]);

        $user->save();
    }
    /** @test */
    public function a_user_can_register(){
        $response =$this->json('POST','api/register',[
            'name'     => 'Bradley Yarrow',
            'email'    => 'support@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in'
        ]);
        $this->assertDatabaseHas('users',['email' => 'support@gmail.com']);
    }

    /** @test */
    public function a_user_can_login(){
        $response =$this->json('POST','api/login',[
            'email'    => 'yarrowbradley@gmail.com',
            'password' => '12345678',
        ]);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in'
        ]);
    }

    /** @test */
    public function an_invalid_user_cannot_login()
    {
        $response = $this->json('POST','api/login', [
                'email'    => 'me@gmail.com',
                'password' => 'dontknowyou'
        ]);
        $response->assertJsonStructure([
                'error',
        ]);
    }
}
