<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CourseRegistrationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_authenticated_user_can_register_one_or_more_course()
    {
        $this->signIn()->withExceptionHandling();
        factory('App\Course',2)->create();
        $response = $this->json('POST',route('course.register'),[
            'courses' => ['1','2']
        ]);
        $response->assertStatus(200);
    }

    /** @test */
    public function an_unauthenticated_user_cannot_register_courses()
    {
        factory('App\Course',2)->create();
        $this->withExceptionHandling();
        $response = $this->json('POST',route('course.register'),[
            'courses' => ['1','2']
        ]);
        $response->assertStatus(401);
    }

    /** @test */
    public function an_authenticated_user_can_see_his_registered_courses()
    {
        $this->signIn()->withExceptionHandling();
        $courses = factory('App\Course',2)->create();
        $response = $this->json('POST',route('course.register'),[
            'courses' => [$courses[0]->id,$courses[1]->id]
        ]);
        $this->withExceptionHandling();

        $response->assertStatus(200);
        $this->json('GET',route('user.courses'))->assertStatus(200);

    }
    /** @test */
    public function an_authenticated_user_cannot_register_to_a_course_twice()
    {
        $this->signIn()->withExceptionHandling();
        $courses = factory('App\Course',2)->create();
        $this->json('POST',route('course.register'),[
            'courses' => [$courses[0]->id,$courses[1]->id]
        ]);
        $this->withExceptionHandling();
        $response = $this->json('POST',route('course.register'),[
            'courses' => [$courses[0]->id,$courses[1]->id]
        ]);

        $response->assertStatus(400);
    }
}
