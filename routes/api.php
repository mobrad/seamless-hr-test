<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'API'],function(){
    Route::post('/register','AuthController@register');
    Route::post('/login','AuthController@login');
    Route::group(['prefix' => 'course'],function(){
        Route::post('/register','CourseRegistrationController@store')->name('course.register');
        Route::get('/create','CourseController@factory')->name('course.create');
        Route::get('/all','CourseController@index')->name('course.all');
    });
    Route::get('/user/courses','CourseRegistrationController@index')->name('user.courses');
    Route::get('/courses/export','CourseController@export')->name('courses.export');
});
