<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Course::class, function (Faker $faker) {
    $name = $faker->jobTitle;
    return [
        //
        "name" =>$name ,
        "course_code" => $faker->unique()->randomNumber(7),
        "description" => $faker->sentence,
        "slug" => Str::slug($name)
    ];
});
