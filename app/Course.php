<?php

namespace App;

use const false;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Course extends Model
{
    //
    protected $guarded = [];
    protected $timestamp = false;
    public static function boot()
    {
        parent::boot();
        static::created(function ($course){
            $course->update(['slug' => $course->name]);
        });

    }
    public function setSlugAttribute($value)
    {
        $slug = Str::slug($value);

        if (static::whereSlug($slug)->exists()){
            $slug = "{$slug}-".$this->id;
        }
        $this->attributes['slug'] = $slug;
    }

}
