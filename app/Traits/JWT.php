<?php
namespace App\Traits;
/**
 * Created by PhpStorm.
 * User: bradley_peexoo
 * Date: 07/03/2020
 * Time: 6:52 AM
 */
//In case another model requires use of JWT
trait JWT
{
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function setPasswordAttribute($password)
    {
        //Hash the password
        if ( !empty($password) ) {
            $this->attributes['password'] = bcrypt($password);
        }
    }
}