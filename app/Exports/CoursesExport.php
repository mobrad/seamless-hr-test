<?php
namespace App\Exports;
use App\Course;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * Created by PhpStorm.
 * User: bradley_peexoo
 * Date: 08/03/2020
 * Time: 2:36 PM
 */
class CoursesExport implements FromCollection
{
    public function collection()
    {
        return Course::all();
    }
}