<?php

namespace App\Http\Controllers;


class BaseController extends Controller
{
    //
    public function sendResponse($result, $message,$view = null)
    {

        if(request()->expectsJson()){
            $response = [
                'success' => true,
                'data'    => $result,
                'message' => $message,
            ];
            return response()->json($response, 200);
        }
    }
    public function sendError($error, $errorMessages = [], $code = 422)
    {
        if(request()->expectsJson()) {
            $response = [
                'success' => false,
                'message' => $error,
            ];
            if (!empty($errorMessages)) {
                $response['data'] = $errorMessages;
            }

            return response()->json($response, $code);
        }
    }
}
