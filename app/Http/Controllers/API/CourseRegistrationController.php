<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\UserCourses;
use Illuminate\Support\Facades\Validator;
use function response;


class CourseRegistrationController extends BaseController
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store()
    {
        $user = auth('api')->user();
        $validator = Validator::make(request('courses'),$this->rule());

        if($validator->fails()){
            return $this->sendError('Validation Error',$validator->errors());
        }
        $courses = request('courses');
        return $this->createCourses($courses,$user);
    }
    public function rule()
    {
        return [
            "courses.*" => 'required'
        ];
    }
    public function createCourses($courses,$user)
    {
        try{
            foreach($courses as $course){
                $attributes = ['user_id' => $user->id,'course_id' => $course];
                if(UserCourses::where($attributes)->exists()){
                    return response()->json([
                        "message" => "cannot register course twice"
                    ],400);
                }
                UserCourses::forceCreate([
                    "user_id" => $user->id,
                    "course_id" => $course
                ]);
            }
        }catch (\Exception $e){
            return $this->sendError("Something went wrong",$e->getMessage(),500);
        }
        return $this->sendResponse($user->courses,"Courses Registered successfully");
    }

    public function index()
    {
        $courses = auth('api')->user()->courses;
        return  $this->sendResponse($courses,"User courses retrieved successfully");
    }

}
