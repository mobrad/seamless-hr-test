<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthController extends BaseController
{
    //
    public function register()
    {
        $data = request()->all();
        $validator = Validator::make($data,$this->rule());
        if($validator->fails()){
            return $this->sendError('Validation Error',$validator->errors());
        }
        unset($data['password_confirmation']);
        //Create th User
        $user = User::forceCreate($data);
        //Login the user
        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'User name or Password invalid'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function rule()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }
}
