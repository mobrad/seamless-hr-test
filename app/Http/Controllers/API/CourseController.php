<?php

namespace App\Http\Controllers\API;

use App\Course;
use App\Exports\CoursesExport;
use App\Http\Controllers\BaseController;
use App\Jobs\CreateCourses;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends BaseController
{
    //
    public function index()
    {
        $courses = Course::all();
        return $this->sendResponse($courses,"Courses retrieved successfully");
    }
    public function factory()
    {
        try{
            dispatch(new CreateCourses());
            return response()->json([
                "message" => "Factory created successful"
            ],200);
        }catch (\Exception $e){
            return $this->sendError("Something went wrong",$e->getMessage(),500);
        }
    }
    public function export()
    {
        return Excel::download(new CoursesExport(), 'courses.xlsx');
    }
}
