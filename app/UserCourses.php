<?php

namespace App;

use const false;
use Illuminate\Database\Eloquent\Model;

class UserCourses extends Model
{
    //
    const CREATED_AT = "date_enrolled";
    public $timestamps = false;

    public function courses()
    {
        return $this->belongsToMany(Course::class,'user_courses','course_id','user_id');
    }

}
