
## Seamless HR Test

Steps to run

- Run "composer install" after pulling project
- Setup database on your local machine
- Create your env file and change your to your values
- Run "php artisan migrate"
- Run "php artisan serve"
- Don't forget to start the queue worker with "php artisan queue:work".
- You can run the unit test using "vendor/bin/phpunit" in quotes



## API Documentation

The api documentation can be found here [documentation](https://documenter.getpostman.com/view/5222641/SzRxVVfT) 

